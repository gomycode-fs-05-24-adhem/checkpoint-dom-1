/*Gestion du bouton like. Comment cela se passe :
1- J'utilise le Dom pour sélectionner tous les éléments qui possèdent la classe qui m'intéresse puis j'utlise le forEach pour lui faire comprenddre que l'action à venir devra être utilisée pour chaque élément. bref étudie l'exemple.
*/

document.querySelectorAll('.like').forEach(element => { element.addEventListener('click', function () {
    element.classList.toggle('liked')
})    
});

//Gestion du panier : déja, je crée des valeurs dans ma div qui contient chacune des cartes où je stocke le prix et un numéro qui correspond à l'identifiant du produit.

//Méthode pour créer une carte pour chaque éléments et récupérer leurs valeurs.
document.querySelectorAll('.btnbuy').forEach(button => {
    button.addEventListener('click', (event) => {
        const productCard = event.target.parentElement;
        const productId = productCard.getAttribute('product-id')
        const productDescription = productCard.getAttribute('texte')
        const productPrice = parseFloat (productCard.getAttribute('product-price'))
        addToCart(productId, productDescription, productPrice)
    }
)
});

//Maintenant, je créé une variable pour mon cardcontainer défini dans le html et je lui donne le même nom (je ne sais pas si c'est obligatoire).
const cardContainer = document.querySelector('.cardContainer')
//Là, je créé un objet cart vide pour pouvoir l'utiliser plus tard dans ma fonction addToCart où il pourra prendre en clé mes trois valeurs.
const cart = {}

//Je créé une fonction addToCart ayant pour paramètres les variables du prix, de l'id et de la description.
function addToCart(productDescription, productId, productPrice) {
    //Je vérifie le cart productid existe, si c'est le cas, je me contente d'incrémenter sa quantité
    if (cart[productId]){
        cart[productId].quantity++
    }
    //sinon, je créé le cartproductid qui est un tableau qui a des clés (noms au choix) dont les valeurs sont la description, le prix et la quantité.
    else{
        cart[productId] = {
            description : productDescription,
            quantity : 1,
            price : productPrice,
        }
        //
        const cartItem = document.createElement('div')
        cartItem.className = 'cart-item';
        cartItem.id = `cart-item-${productId}`; //Toujours utiliser les backticks dans ce genre de situations.
        cartItem.innerHTML=`
        <p>${productDescription}</p>
        <p class="quantity">Quantité : <span class="quantity-value">${cart[productId].quantity}</span></p>
        <p class="price"> Prix : <span class="prix">${(cart[productId].quantity*cart[productId].price).toFixed(3)}</span></p>
        <button class="increment">+</button>
        <button class="decrement">-</button>
        <button class="delete">Supprimer</button>
        `;
        cardContainer.appendChild(cartItem)
        cartItem.querySelector('.increment').addEventListener('click', () => updateQuantity(productId, 1))
        cartItem.querySelector('.decrement').addEventListener('click', () => updateQuantity(productId, -1))
        cartItem.querySelector('.delete').addEventListener('click', () => deleteItem((productId)))
    };
    updateCartItem(productId)
}

//Gestion de la fonction updateQuantity.
function updateQuantity(productId, change){
    if (cart[productId]){
        cart[productId].quantity += change
        if (cart[productId].quantity <= 0){
            deleteItem(productId)
        }
        else{
            updateCartItem(productId)
        }
    }
}

//Gestion de la fonction updateCartItem.
function updateCartItem (productId){
    const cartItem = document.getElementById(`cart-item-${productId}`);
    cartItem.querySelector('.quantity-value').textContent = cart[productId].quantity;
    cartItem.querySelector('.prix').textContent = (cart[productId].quantity*cart[productId].price).toFixed(2);
}

//Gestion du bouton delete
function deleteItem(productId){
    delete cart[productId]
    document.getElementById(`cart-item-${productId}`).remove()
}