// Gestion des likes
document.querySelectorAll('.like-button').forEach(button => {
    button.addEventListener('click', () => {
        button.classList.toggle('liked');
    });
});

// Gestion des achats
document.querySelectorAll('.buy-button').forEach(button => {
    button.addEventListener('click', (event) => {
        const productCard = event.target.parentElement;
        const productId = productCard.getAttribute('data-product-id');
        const productDescription = productCard.querySelector('p').textContent;
        const productPrice = parseFloat(productCard.getAttribute('data-product-price'));
        addToCart(productId, productDescription, productPrice);
    });
});

const cartContainer = document.getElementById('cartContainer');
const cart = {};

function addToCart(productId, productDescription, productPrice) {
    if (cart[productId]) {
        cart[productId].quantity++;
    } else {
        cart[productId] = { description: productDescription, quantity: 1, price: productPrice };
        const cartItem = document.createElement('div');
        cartItem.className = 'cart-item';
        cartItem.id = `cart-item-${productId}`;
        cartItem.innerHTML = `
            <p>${productDescription}</p>
            <p class="quantity">Quantité: <span class="quantity-value">${cart[productId].quantity}</span></p>
            <p class="price">Prix: <span class="price-value">${(cart[productId].price * cart[productId].quantity).toFixed(2)}</span>€</p>
            <button class="increment">+</button>
            <button class="decrement">-</button>
            <button class="delete">Supprimer</button>
        `;
        cartContainer.appendChild(cartItem);

        cartItem.querySelector('.increment').addEventListener('click', () => updateQuantity(productId, 1));
        cartItem.querySelector('.decrement').addEventListener('click', () => updateQuantity(productId, -1));
        cartItem.querySelector('.delete').addEventListener('click', () => deleteItem(productId));
    }
    updateCartItem(productId);
}

function updateQuantity(productId, change) {
    if (cart[productId]) {
        cart[productId].quantity += change;
        if (cart[productId].quantity <= 0) {
            deleteItem(productId);
        } else {
            updateCartItem(productId);
        }
    }
}

function updateCartItem(productId) {
    const cartItem = document.getElementById(`cart-item-${productId}`);
    cartItem.querySelector('.quantity-value').textContent = cart[productId].quantity;
    cartItem.querySelector('.price-value').textContent = (cart[productId].price * cart[productId].quantity).toFixed(2);
}

function deleteItem(productId) {
    delete cart[productId];
    document.getElementById(`cart-item-${productId}`).remove();
}
